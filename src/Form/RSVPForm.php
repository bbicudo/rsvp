<?php

namespace Drupal\rsvplist\Form;

/**
 * @file
 * Contains \Drupal\rsvplist\Form\RSVPForm.
 */

use Drupal\Component\Utility\EmailValidator;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a RSVP email Form.
 */
class RSVPForm extends FormBase {
  use \Drupal\Core\StringTranslation\StringTranslationTrait;

  /**
   * The Route Match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The email validator.
   *
   * @var \Drupal\Component\Utility\EmailValidator
   */
  protected $emailValitador;

  /**
   * The Database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The Messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Current User.
   *
   * @var Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a RSVPForm object.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The Route Match.
   * @param \Drupal\Component\Utility\EmailValidator $email_validator
   *   The email validator.
   * @param \Drupal\Core\Database\Connection $database_connection
   *   The database connection.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\user\UserStorageInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   */
  public function __construct(RouteMatchInterface $route_match, EmailValidator $email_validator, Connection $database_connection, MessengerInterface $messenger, UserStorageInterface $entity_type_manager, AccountProxyInterface $current_user) {
    $this->routeMatch = $route_match;
    $this->emailValitador = $email_validator;
    $this->database = $database_connection;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('email.validator'),
      $container->get('database'),
      $container->get('messenger'),
      $container->get('entity_type.manager')->getStorage('user'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'rsvplist_email_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $node = $this->routeMatch->getParameter('node');
    $nid = 0;

    if (isset($node)) {
      $nid = $node->id();
    }

    $form['email'] = [
      '#title' => $this->t('Email Adress'),
      '#type' => 'textfield',
      '#size' => 25,
      '#description' => $this->t("We'll send updates to the email address you provide"),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('RSVP'),
    ];

    $form['nid'] = [
      '#type' => 'hidden',
      '#value' => $nid,
    ];

    return $form;
  }

  /**
   * Validates data to check if it's a valid e-mail.
   *
   * @param array $form
   *   Receives (referenced) form as parameter.
   * @param \FormStateInterface $form_state
   *   Receives form state as parameter.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $value = $form_state->getValue('email');
    if ($value == !$this->emailValitador->isValid($value)) {
      $form_state->setErrorByName('email', $this->t('The email address %mail is not valid', ['%mail' => $value]));
      return;
    }
    $node = $this->routeMatch->getParameter('node');
    // Check if email is already set for this node.
    $select = Database::getConnection()->select('rsvplist', 'r');
    $select->fields('r', ['nid']);
    $select->condition('nid', $node->id());
    $select->condition('mail', $value);
    $results = $select->execute();
    if (!empty($results->fetchCol())) {
      // We found a row with this nid and mail.
      $form_state->setErrorByName('email', $this->t('The address %mail is already subscribed to this list.', ['%mail' => $value]));
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user = $this->entityTypeManager->load($this->currentUser->id());
    $this->database->insert('rsvplist')->fields([
      'mail' => $form_state->getValue('email'),
      'nid' => $form_state->getValue('nid'),
      'uid' => $user->id(),
      'created' => time(),
    ])
      ->execute();

    $this->messenger->addMessage($this->t('Thank you for your RSVP, you are on the list for the event!'));
  }

}
