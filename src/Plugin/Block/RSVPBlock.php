<?php

namespace Drupal\rsvplist\Plugin\Block;

/**
 * @file
 * Contains \Drupal\rsvplist\Plugin\Block\RSVPBlock.
 */

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\rsvplist\EnablerService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an 'RSVP' List Block.
 *
 * @Block(
 *   id = "rsvp_block",
 *   admin_label = @Translation("RSVP Block"),
 * )
 */
class RSVPBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The Form Builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The Route Matcher.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The Route Matcher.
   *
   * @var \Drupal\rsvplist\EnablerService
   */
  protected $rsvpListEnabler;

  /**
   * Constructs a RSVPBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The Form Builder.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The Route Matcher.
   * @param \Drupal\rsvplist\EnablerService $rsvp_list_enabler
   *   The RSVP list Enabler.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder, RouteMatchInterface $route_match, EnablerService $rsvp_list_enabler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
    $this->routeMatch = $route_match;
    $this->rsvpListEnabler = $rsvp_list_enabler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder'),
      $container->get('current_route_match'),
      $container->get('rsvplist.enabler')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function build() {
    return $this->formBuilder->getForm('Drupal\rsvplist\Form\RSVPForm');
  }

  /**
   * Defines block access.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Receives logged on as parameter.
   *
   * @return bool
   *   Returns true if user haz block access permission. Either, returns false.
   */
  public function blockAccess(AccountInterface $account) {
    $node = $this->routeMatch->getParameter('node');

    /**
     * @var \Drupal\rsvplist\EnablerService $enabler
     */
    $enabler = $this->rsvpListEnabler;
    if ($node) {
      if ($enabler->isEnabled($node)) {
        return AccessResult::allowedIfHasPermission($account, 'view rsvplist');
      }
    }
    return AccessResult::forbidden();
  }

}
